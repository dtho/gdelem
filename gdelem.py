#!/usr/bin/python

# For handling Google Drive API
from __future__ import print_function # this must be at start of file

# For checking the python version
import sys
if sys.version_info[0] > 2:
    raise Exception("Must be using Python 2")

# For handling Google Drive API (cont'd)
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# For handling deletion
import datetime
from dateutil import parser

# For handling CLI arguments
import argparse

# Empty the trash?
emptyTrashP=False
parser=argparse.ArgumentParser(description="This is gdelem")
parser.add_argument("--trash",
                    help="delete the contents of the Trash",
                    action="store_true")
args=parser.parse_args()
if args.trash==True:
    emptyTrashP=True
    
# If this value is modified, delete the file token.pickle. 
SCOPES = ['https://www.googleapis.com/auth/drive']

# Folders to delete files from
# - the folder id value can be taken directly from the browser Google Drive URL for the folder
#   example: https://drive.google.com/drive/u/0/folders/1g8FFyjMDH8m9QVFv20Iorv9O7YyWrABC
FolderIds=[ "1g8FFyjMDH8m9QVFv20Iorv9O7YyWrABC" ]

# Selecting files to delete by
# - if age is None, then delete all
SelectBy={
    #"age": { 'days': 4 }        # select content older than NN days
    "age": None
}

# If False, then just mock
DeleteP=True

# Verbose output for files?
PrintFilesP=False

def deleteTrashFiles(service):
    """ Act on single folder """
    # Call v3 API: really delete the files in the Trash
    print('Deleting files in Trash')
    service.files().emptyTrash().execute()

def actOnFolder(service,folderId):
    """ Act on single folder """
    # Call v3 API - list files in a folder    
    # Don't use mimeType = \'application/vnd.google-apps.file --- it does not work as expected
    #q = '(mimeType = \'application/vnd.google-apps.file\') and (\'{0}\' in parents)'.format(dirId)
    q = '(\'{0}\' in parents)'.format(folderId)
    print(q)
    MaximumPageSize=1000        # 1000 is maximum page size with Google Drive API
    results = service.files().list(
        pageSize=MaximumPageSize,
        q=q,
        fields="nextPageToken, files(id, name, createdTime)").execute()
    items = results.get('files', [])    
    #
    # Emit items to console
    #
    if not items:
        print('No files found.')
    else:
        print('Files:')
        print(len(items))
        if PrintFilesP:
            for item in items:
                print('Item:')
                print(item)
                if "parents" in item:
                    print(u'name: {0} id: {1} parents: {2}'.format(item['name'], item['id'], item["parents"]))
                else:
                    print(u'name: {0} id: {1} created: {2}'.format(item['name'], item['id'], item["createdTime"]))
    #
    # Delete by age
    #
    if DeleteP and "age" in SelectBy:
        i=1;
        for item in items:
            print(i)
            i=i+1
            now=datetime.datetime.now()
            nowNaive=now.replace(tzinfo=None)
            ctime=item["createdTime"] # e.g., 2020-09-29T23:00:21.021Z
            then=parser.isoparse(ctime)
            thenNaive=then.replace(tzinfo=None)
            timeDelta=datetime.timedelta(**SelectBy["age"])
            if (nowNaive - thenNaive) > timeDelta:
                print("Delete!")
                fileId=item["id"]
                # note that .delete().execute() moves file to Trash
                deletedFile = service.files().delete(fileId=fileId).execute()
            else:
                print("Keep!")
        # Now really delete the files
        service.files().emptyTrash().execute()

def main():
    """Delete specified files. """
    creds = None
    # The file token.pickle stores the user's access and refresh
    # tokens, and is created automatically when the authorization flow
    # completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('drive', 'v3', credentials=creds)
    if emptyTrashP:
        deleteTrashFiles(service)
    else:
        #
        # Do stuff below for *each* folder
        #
        for folderId in FolderIds:
            actOnFolder(service,folderId)
        
if __name__ == '__main__':
    main()
